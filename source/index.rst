.. Huayu's Blogs documentation master file, created by
    sphinx-quickstart on Wed Jan 11 18:39:26 2023.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to Huayu's Blogs
=========================================



Operating System Development:
-----------------------------
This section is the blogs for Operating System Leanring and Development.

.. toctree::
    :maxdepth: 2

    os/index

Graphic API Development:
-----------------------------
This section is the blogs for Operating System Leanring and Development.

.. toctree::
    :maxdepth: 2

    graphic-api/index


Indices and tables
==================

* :ref:`genindex`
