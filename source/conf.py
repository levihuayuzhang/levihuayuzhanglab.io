# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Huayu's Blogs"
copyright = '2023, Huayu Zhang'
author = 'Huayu Zhang'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# extensions = ['rst2pdf.pdfbuilder']

templates_path = ['_templates']
exclude_patterns = []

html_static_path = ['_static']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output


# html_theme = 'alabaster'
# html_sidebars = { '**': ["about.html", 'searchbox.html', 'localtoc.html', 'sourcelink.html']}


# html_theme = 'furo'
# pygments_style = "sphinx"
# pygments_dark_style = "monokai"

html_theme = 'piccolo_theme'
html_logo = './_static/hacker.png'
html_favicon = './_static/hacker.png'

# pdf_documents = [('index', u'blogs', u'blogs', u'Huayu Zhang'),]
#   # index - master document
#   # rst2pdf - name of the generated pdf
#   # Sample rst2pdf doc - title of the pdf
#   # Your Name - author name in the pdf