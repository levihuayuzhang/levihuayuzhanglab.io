Huayu's Blogs
=================

This is the blog source code repo of `Levi`_.
The web source is holding at `levihuayuzhang.gitlab.io <https://levihuayuzhang.gitlab.io>`_.

This blog site using `reStructuredText`_ and `sphinx`_ for wirting.

And using `GitLab's`_ `CI/CD`_ and Pages_ for generating and holding web source.


.. _Levi: https://github.com/levihuayuzhang
.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _sphinx: https://www.sphinx-doc.org/en/master/
.. _GitLab's: https://about.gitlab.com/
.. _CI/CD: https://docs.gitlab.com/ee/ci/
.. _Pages: https://docs.gitlab.com/ee/user/project/pages/
